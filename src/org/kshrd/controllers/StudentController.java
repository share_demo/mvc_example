package org.kshrd.controllers;

import org.kshrd.models.dao.StudentData;
import org.kshrd.models.dto.Student;
import org.kshrd.views.StudentView;

/**
 * Created by temchannat on 5/8/18.
 */
public class StudentController {

    Student student;
    StudentData studentData = new StudentData();
    StudentView studentView = new StudentView();

    public void updateStudentView() {
        student = studentData.studentList();
        studentView.printStudentInformation(student);

    }

}
