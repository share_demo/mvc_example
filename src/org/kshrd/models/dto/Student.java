package org.kshrd.models.dto;

/**
 * Created by temchannat on 5/8/18.
 */

// DTO: Data Transfer Object
public class Student {

    String rollNumber;
    String name;

    public Student(String rollNumber, String name) {
        this.rollNumber = rollNumber;
        this.name = name;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
