package org.kshrd.models.dao;

import org.kshrd.models.dto.Student;

/**
 * Created by temchannat on 5/8/18.
 */

// DAO: Data Access Object
public class StudentData {

    public Student studentList() {
        Student student = new Student("SR001",
                "Chan Chhaya");
        return student;
    }

}
