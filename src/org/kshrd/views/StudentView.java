package org.kshrd.views;

import org.kshrd.models.dto.Student;

/**
 * Created by temchannat on 5/8/18.
 */
public class StudentView {

    public void printStudentInformation(Student student) {

        System.out.println(student.getRollNumber()
                + "\t" + student.getName());

    }

}
